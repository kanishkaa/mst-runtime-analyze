package main

import (
	"errors"
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/yourbasic/graph"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	if os.Args[1] == "solve" {
		solveOne(os.Args[2:])
	} else if os.Args[1] == "compare" {
		compareInstances(os.Args[2:])
	} else {
		panic("main: expected either solve or compare")
	}
}

// solve and display mst for a randomly generated graph
func solveOne(args []string) {
	instanceOptions, err := parseSolveOneOptions(args)
	if err != nil {
		panic(err.Error())
	}
	grph := generate(instanceOptions)
	mst := graph.MST(grph)
	printDot(grph, mst)
}

// compare compute time to find MST for three randomly generated graphs
func compareInstances(args []string) {
	instanceOptionsSet, err := parseCompareInstancesOptions(args)
	if err != nil {
		panic(err.Error())
	}
	for _, instanceOptions := range instanceOptionsSet {
		grph := generate(instanceOptions)
		start := time.Now()
		graph.MST(grph)
		elapsed := time.Since(start)
		fmt.Printf("%v nodes, %v connected, %v: %vms\n",
			instanceOptions.nodeCnt, instanceOptions.pctConnected, instanceOptions.distribName,
			elapsed.Milliseconds())
	}
}

// parse options for running compareInstances
func parseCompareInstancesOptions(args []string) ([3]instanceOptions, error) {
	var instanceOptions [3]instanceOptions
	if len(args) < (3 * 3) {
		return instanceOptions, errors.New("parseCompareInstanceOptions: too few arguments")
	}
	// TODO: read instances from stdin
	for i := 0; i < 3; i++ {
		start := i * 3
		var err error
		instanceOptions[i], err = parseInstanceOptions([...]string{args[start], args[start+1], args[start+2]})
		if err != nil {
			return instanceOptions, err
		}
	}
	return instanceOptions, nil
}

// parse options for running solveOne
func parseSolveOneOptions(args []string) (instanceOptions, error) {
	if len(args) < 3 {
		return instanceOptions{}, errors.New("parseSolveOneOptions: too few arguments")
	}
	return parseInstanceOptions([...]string{args[0], args[1], args[2]})
}
