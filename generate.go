package main

import (
	"errors"
	"math/rand"
	"strconv"

	"github.com/yourbasic/graph"
)

// options used to generate a random graph
type instanceOptions struct {
	nodeCnt      int
	pctConnected float64
	wghtDistrib  func() int64
	distribName  string
}

// parse an instanceOptions from provided strings
func parseInstanceOptions(args [3]string) (instanceOptions, error) {
	// TODO: stricter validation on allowed values
	nodeCnt, err := strconv.ParseInt(args[0], 10, 0)
	if err != nil {
		return instanceOptions{}, err
	}
	pctConnected, err := strconv.ParseFloat(args[1], 64)
	if err != nil {
		return instanceOptions{}, err
	}
	wghtDistrib, distribName, err := parseDistribution(args[2])
	if err != nil {
		return instanceOptions{}, err
	}
	return instanceOptions{int(nodeCnt), pctConnected, wghtDistrib, distribName}, nil
}

// resolve the weight distribution function based on the provided name
func parseDistribution(name string) (func() int64, string, error) {
	if name == "uniform" {
		return uniformDistribution, name, nil
	} else if name == "lopsided" {
		return lopsidedDistribution, name, nil
	} else {
		return nil, "", errors.New("parseDistribution: expected uniform or lopsided")
	}
}

// weights are integers in the interval [1, weightUpperBound]
const weightUpperBound = int64(10)

// generate weights uniformly within allowed values
func uniformDistribution() int64 {
	return rand.Int63n(weightUpperBound) + 1
}

// generate weights with a lopsided distribution
func lopsidedDistribution() int64 {
	return 1
}

// generate a random graph with provided options
func generate(instanceOptions instanceOptions) graph.Iterator {
	grph := graph.New(instanceOptions.nodeCnt)
	// there is no guarantee that the resulting graph is connected
	for n1 := 0; n1 < instanceOptions.nodeCnt; n1++ {
		for n2 := n1 + 1; n2 < instanceOptions.nodeCnt; n2++ {
			if rand.Float64() <= instanceOptions.pctConnected {
				// the implementation of AddBoth appears to prevent creating a
				// multigraph if AddBoth is called multiple times with the same nodes
				grph.AddBothCost(n1, n2, instanceOptions.wghtDistrib())
			}
		}
	}
	return grph
}
