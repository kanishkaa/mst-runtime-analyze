package main

import (
	"fmt"

	"github.com/yourbasic/graph"
)

// encode and print the graph in the DOT format which is
// accepted by the graphviz tool
func printDot(grph graph.Iterator, mst []int) {
	fmt.Println(" strict graph G { ")
	for n := 0; n < grph.Order(); n++ {
		hasEdge := false
		grph.Visit(n, func(w int, c int64) bool {
			hasEdge = true
			// the undirected graph is represented with 2 directed edges,
			//  so we only print out 1 of the 2 directed edges by
			//  ignoring edges going from higher node ids to lower node ids
			if n < w {
				if containsEdge(mst, n, w) {
					fmt.Println("  ", n, "--", w, "[len=", c, ", color=green]")
				} else {
					fmt.Println("  ", n, "--", w, "[len=", c, "]")
				}
			}
			return false
		})
		if !hasEdge {
			fmt.Println("  ", n)
		}
	}
	fmt.Println(" }")
}

// determine whether the mst contains an edge between n1 and n2
func containsEdge(mst []int, n1 int, n2 int) bool {
	return mst[n1] == n2 || mst[n2] == n1
}
